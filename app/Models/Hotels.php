<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * @property integer $id
 * @property string $name
 * @property string $created_at
 * @property string $updated_at
 * @property UserHasHotel[] $userHasHotels
 */
class Hotels extends Model
{
    use \Backpack\CRUD\app\Models\Traits\CrudTrait;
    /**
     * The "type" of the auto-incrementing ID.
     *
     * @var string
     */
    protected $keyType = 'integer';

    /**
     * @var array
     */
    protected $fillable = ['name', 'created_at', 'updated_at'];

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function userHasHotels()
    {
        return $this->hasMany('App\UserHasHotel');
    }
}
